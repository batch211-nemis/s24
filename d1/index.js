//console.log("Hello B211!");

// console.log("Hello World!");

//ES6 Updates

//ES6 is one of the latest versions of writing JS and in fact is one of the major update to JS
//let, const - are ES6 updates, these are the new standards of creating variables

//Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);//64

const secondNum = Math.pow(8,2);
console.log(secondNum);


let string1 = "fun";
let string2 = "Bootcamp";
let string3 = "Coding";
let string4 = "JavaScript";
let string5 = "Zuitt";
let string6 = "Learning";
let string7 = "love";
let string8 = "I";
let string9 = "is";
let string10 = "in";
/*
	Mini Activity
	1. Create new variables called sentence1 and sentence2
	2. Concatentate and save a resulting string into sentence1
	3. Concatentate and save a resulting string into sentence2
		log both variables in the console and take a screenshot
		The sentences must have spaces and punctuation
*/

// let sentence1 = string8 + " " + string7 + " " + string5 + " " + string3 + " " + string2 + "!";
// let sentence2 = string6 + " " + string4 + " " + string9 + " " + string1 + "!";
// console.log(sentence1);
// console.log(sentence2);

// "" , '' - string literals

//Template Literals
	//allow us to create strings using `` and easily embed JS expression in it
		/*
			it allows us to write strings without using the concatentation operator (+)
			greatly helps with code readability
		*/

let sentence1 = `${string8} ${string7} ${string5} ${string3} ${string2}!`;
console.log(sentence1);

/*
	${} is a placeholder that is used to embed JS expressions when creating strings using Template Literals

*/

let name = "John";

//Pre-Template Literal String
//("") or ('')

let message = "Hello" + name + '! Welcome to programming!';

//Strings using template literals
//(``) backticks

message = `Hello ${name}! Welcome to programming.`;
console.log(message);

//Multi-line using Template Literals

const anotherMessage = `

${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.

`
console.log(anotherMessage);

let dev = {
	name: "Peter",
	lastName: "Parker",
	occupation: "web developer",
	income: 50000,
	expenses: 60000
};

console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is ${principal * interestRate}`);

//Array Destructuring

/*
	-Allows us to unpack elements in arrays into distinct variables


	Syntax:

		let/const [variableNameA, variableNameB, variableNameC] = array;

*/

const fullName = ["Juan", "Dela", "Cruz"];

//Pre-Array Destructing
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);


//Array Destructuring

const [firstName, middleName,lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);


console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`);

//Object Destructuring

/*
		Allows us to unpack properties of objects into distinct variab;es

		Syntax
		let/const {propertyNameA,propertyNameB,propertyNameC} = object

*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

//Pre-Object Destructuring
//we can acces them using .  or []

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

//Object Destructuring

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

//Arrow Functions

/*
	-Compact alternative syntax to traditional functions

*/

const hello = () => {
	console.log("Hello world");
}

// const hello = function hello1(){
// 	console.log("Hello world");
// }

hello();

//Pre-Arrow Function and Template Literals
/*
	Syntax:

	function functionName(parameterA,parameterB,parameterC){
		console.log();
	}

*/

//Arrow Function

/*
	Syntax:
	let/const variableName = (parameterA,parameterB,parameterC) => {
		console.log;
	}


*/

const printFullName = (firstName, middleInitial,lastName) => {
	console.log(`${firstName} ${middleInitial},${lastName}`)
};

printFullName("John","D","Smith");

//Arrow Functions with loops

//Pre-arrow function

const students = ["John","Jane","Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`);
});

//Arrow Function

students.forEach((student)=>{
	console.log(`${student} is a student.`)
});

//Implicit Return Statement
/*
	There are instances when you can omit the "return" statement
	This works because even without the "return" statement JS IMPLICITLY adds it for the result of the function
	

*/

//Pre-arrow function

// const add = (x,y) => {
// 	return x + y;
// }

	//{} in an arrow function are code blocks. if an arrow fucntion has a {} or code block, we're going to need to use a return

	//implicit return will only work on arrow functions without {}

const add = (x,y) => x + y;

let total = add(1,2);
console.log(total);

/*
		Mini-Activity 2
		create a subtract, multiply, and divide arrow function
		use 1 and 2 as your arguments
		log the total in your console.
*/

const subtract = (x,y) => x - y;

let totalSub = subtract(1,2);
console.log(totalSub);

const multiply = (x,y) => x * y;

let totalM = multiply(1,2);
console.log(totalM);

const divide = (x,y) => x / y;

let totalD = divide(1,2);
console.log(totalD);


//Default Function Argument Value
//provide a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good evening, ${name}`;
};

console.log(greet());
console.log(greet("John"));

//Class-Based Object Blueprint
/*
	Allows creation/instantiation of objects using classes as blueprints

*/

//Creating a class

/*
	the constructor is a special method of a class for creating/initializing an object for that class
	Syntax

		class Classname {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	//Instantiating an object
/*
	the "new" operator creates/instantiates a new object with the given arguments as the value of its properties

*/
	// let/const variableName = new className();

	/*
		creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't reassign it with another data type

	*/

	const myCar = new Car();

	console.log(myCar);//undefined

	myCar.brand = "Ford";
	myCar.name = "Everest";
	myCar.year = "1996";

	console.log(myCar);

	const myNewCar = new Car("Toyota","Vios",2021);

	console.log(myNewCar);

	/*
		Mini-Actity 3:
		create a Character Class Constructor
			name:
			role:
			strength:
			weakness:

		create 2 new Characters out of the class constructor and save it in their respecitive variables
		-log the variables in the console.

	*/

	class Character {
		constructor(name,role,strength,weakness){
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
		}
	}

	const starlightAnya = new Character("Stralight Anya", "Mage","cuteness","Mama and Papa");
	console.log(starlightAnya);
