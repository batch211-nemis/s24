//console.log("Hello B211!");

/*
	Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
*/

/*
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
*/
	let num=2;
	const getCube = num ** 3;
	


/*

// Template Literals
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/
	
	console.log(`The cube of ${num} is ${getCube}`);


	const address = ["258", "Washinton Ave NW", "California", "90011"];
	const [houseNo, city, country, countryNo] = address;

	console.log(`I live at ${houseNo} ${city}, ${country}, ${countryNo}`);

/*
// Object Destructuring
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

	const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
};

	const {name, type, weight, measurement} = animal;

	console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}`);

	
/*
// Arrow Functions
9. Create an array of numbers.
*/
	
	const numArr = [1, 2, 3, 4, 5];

	

/*
// A
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
// B
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/
	
	numArr.forEach((numbers)=>{
		console.log(`${numbers}`);
	});
	
	const add = (total,numbers) => total + numbers;
	/*
	function myFunc(total, num) {
  		return total + num;
	}*/

	let reduceNumber = numArr.reduce(add);
	console.log(reduceNumber);

/*
// Javascript Objects
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

	class Dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

	const myDog = new Dog();


	myDog.breed = "Miniature Dachshund";
	myDog.name = "Frankie";
	myDog.age = "5";

	console.log(myDog);





/*
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s24 activity code".
16. Add the link in Boodle.

*/

